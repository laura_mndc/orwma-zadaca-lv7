package hr.lmandic.orwmalv7dz;

import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;

import androidx.recyclerview.widget.RecyclerView.ViewHolder;

public class NameViewHolder extends ViewHolder implements View.OnClickListener {

    private TextView liTextView;
    private ImageButton ibtnRemove;
    private CustomClickListener clickListener;


    public NameViewHolder(@NonNull View itemView, CustomClickListener listener) {
        super(itemView);
        this.clickListener = listener;
        liTextView = itemView.findViewById(R.id.liTextView);
        ibtnRemove = itemView.findViewById(R.id.ibtnRemove);
        ibtnRemove.setOnClickListener(this);

    }

    public void setName(String name) {
        liTextView.setText(name);

    }

    @Override
    public void onClick(View v) {
        int position = getAdapterPosition();
        clickListener.onClick(position);
    }

}