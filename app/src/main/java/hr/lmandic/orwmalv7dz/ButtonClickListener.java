package hr.lmandic.orwmalv7dz;
public interface ButtonClickListener {
    void onButtonClicked(String message);
}
