package hr.lmandic.orwmalv7dz;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements ButtonClickListener {
    private MessageFragment mMessageFragment;
    private ViewPager mViewPager;
    private TabLayout mTabLayout;
    Random rand;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i("LauraM","usao oncreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.i("LauraM","prosao setcontentview");

        rand = new Random();
        initViews();
        setUpViewPager();
        Log.i("LauraM","prosao oncreate");

    }

    private void setUpViewPager() {
        List<Fragment> fragmentList=new ArrayList();
        mMessageFragment=new MessageFragment();
        fragmentList.add(new InputFragment());
        fragmentList.add(mMessageFragment);
        fragmentList.add(new ModularFragment(rand.nextInt(2)));
        fragmentList.add(new ModularFragment(rand.nextInt(2)));

        SlidePagerAdapter adapter=new SlidePagerAdapter(getSupportFragmentManager(),fragmentList);
        mViewPager.setAdapter(adapter);
        mTabLayout.setupWithViewPager(mViewPager);
        mViewPager.setPageTransformer(false, new ViewPager.PageTransformer() {
            @Override
            public void transformPage(@NonNull View page, float position) {
                page.setRotationY(position*-30);
            }
        });


    }



    private void initViews() {
        mViewPager=findViewById(R.id.viewPager);
        mTabLayout=findViewById(R.id.tabLayout);
    }

    @Override
    public void onButtonClicked(String message) {
        mMessageFragment.displayMessage(message);
    }


}