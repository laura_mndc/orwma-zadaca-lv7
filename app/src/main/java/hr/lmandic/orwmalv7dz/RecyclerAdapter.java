package hr.lmandic.orwmalv7dz;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;



import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<NameViewHolder> {

    private List<String> dataList=new ArrayList<>();
    private CustomClickListener clickListener;

    public RecyclerAdapter(CustomClickListener clickListener)
    {
        this.clickListener=clickListener;
    }
    @NonNull
    @Override
    public NameViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View listItemView= LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new NameViewHolder(listItemView, clickListener);

    }

    @Override
    public void onBindViewHolder(@NonNull NameViewHolder holder, int position) {
        holder.setName(dataList.get(position));
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
    public void addData(List<String> data){
        dataList.clear();
        dataList.addAll(data);
        notifyDataSetChanged();
    }
    public void addNewCell(String name, int position){
        if(dataList.size()>=position){
            dataList.add(position,name);
            notifyItemInserted(position);
        }
    }
    public void removeCell(int position){
        if(dataList.size()>position){
            dataList.remove(position);
            notifyItemRemoved(position);
        }
    }
}
