package hr.lmandic.orwmalv7dz;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

public class ModularFragment extends Fragment implements CustomClickListener{
    int choice;
    private RecyclerView recycler;
    private RecyclerAdapter adapter;
    private EditText etNewName;
    private Button bAdd;
    public ModularFragment(int choice) {
        this.choice=choice;
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if(choice==0){
        return inflater.inflate(R.layout.fragment_image, container, false);
        }
        else{
            View recyclerView=inflater.inflate(R.layout.fragment_recycler, container, false);
            setupRecycler(recyclerView);
            setupRecyclerData();
            etNewName=(EditText) recyclerView.findViewById(R.id.etNewName);
            bAdd=(Button) recyclerView.findViewById(R.id.btnAdd);
            bAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addCell();
                }
            });
            return recyclerView;
        }
    }
    public void addCell(){
        String newName=etNewName.getText().toString();
        etNewName.getText().clear();
        adapter.addNewCell(newName, adapter.getItemCount());
    }



    private void setupRecyclerData() {
        List<String> data=new ArrayList<>();
        data.add("Ana");
        data.add("Marko");
        data.add("Pero");
        adapter.addData(data);
    }

    private void setupRecycler(View recyclerView) {


        recycler=recyclerView.findViewById(R.id.recyclerView);

        LinearLayoutManager mng= new LinearLayoutManager(getActivity());

        recycler.setLayoutManager(mng);

        adapter=new RecyclerAdapter(this);

        recycler.setAdapter(adapter);

    }
    @Override
    public void onClick(int index) {

        adapter.removeCell(index);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

}