package hr.lmandic.orwmalv7dz;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

public class InputFragment extends Fragment implements TextWatcher {
    private String mMessageString = "...";
    private EditText mEditText;
    private Button mSubmitButton;
    private ButtonClickListener mButtonClickListener;
    private MessageFragment mMessageFragment;
    ViewPager viewPager;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_input, container, false);
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mEditText = view.findViewById(R.id.etMessage);
        mSubmitButton = view.findViewById(R.id.btSubmit);
        viewPager =(ViewPager)getActivity().findViewById(R.id.viewPager);
        setUpListeners();
    }
    private void setUpListeners() {
        mSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mButtonClickListener.onButtonClicked(mMessageString);
                viewPager.setCurrentItem(1);

            }
        });
        mEditText.addTextChangedListener(this);
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ButtonClickListener) {
            this.mButtonClickListener = (ButtonClickListener) context;
        }
    }
    @Override
    public void onDetach() {
        super.onDetach();
        this.mButtonClickListener = null;
    }
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }
    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }
    @Override
    public void afterTextChanged(Editable s) {
        mMessageString = s.toString();
    }
}
